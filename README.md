Documentation about available open source RISC-V core, and related FPGA tools, general FPGA or RISC-V related open source tools. Some rare closed source tools are also given in border case, where open source part is not finished.

For now there is just a MarkDown formated file, [RISC-V_FPGA.md](RISC-V_FPGA.md)

Main repository: https://framagit.org/popolon/risc-v_and_fpga

This work is released on the WTFPL license, so you can do what you want with this, but I would be really happy if you give me credits of my work.

License:  [WTFPL](http://www.wtfpl.net/) (French version: [LPRAB](http://sam.zoy.org/lprab/))
Author: Popolon, november 2021

