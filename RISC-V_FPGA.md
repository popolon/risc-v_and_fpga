Here is a non-exhaustive list of RISC-V open cores, some tools related to them, and some still intersting, closed or not know if open ones cores. Latest version can be found at [https://framagit.org/popolon/risc-v_and_fpga/](https://framagit.org/popolon/risc-v_and_fpga/). Information I gathered here are sometime not easy to find, and there could have some errors. This is mainly to have at least a starting point database

* [RISC-V fundation now maintain its own list too](https://github.com/riscvarchive/riscv-cores-list)
* [Free Silicon Conferences](https://peertube.f-si.org/accounts/root/videos) include interesting subjects around Open Soruce Silicon

- [OpenCores](#OpenCores)
    - [TinyCores](#TinyCores)
    - [Microcontrollers](#Microcontrollers)
    - [Microprocessors](#Microprocessors)
    - [Accelerators](#Accelerators)
- [Simulators](#Simulators)
- [Emulators](#Emulators)
- [Tools](#Tools)
- [MicrocontrollerOS](#MicrocontrollerOS)
- [MicroprocessorOS](#MicroprocessorOS)
- [ExtensionsNomenclature](#ExtensionsNomenclature)
- [MoreDoc][#MoreDoc)

# OpenCores

## TinyCores

Cores that have for objective the minimum size

[nanoV](https://github.com/MichaelBell/nanoV)
- Goal: A bit serial RISC-V core and integrated SPI memory controller, for minimal area RISC-V computing. Privilege the use of FRAM for data peristence.
- Language: Verilog
- License: Apache 2.0
- Author: Michael Bell

[tinyQV](https://github.com/MichaelBell/tinyQV)
- Goal: A 4 bit at a time RV32 processor, similar to nanoV but optimized for QSPI RAM and Flash instead of SPI FRAM. The aim of this design is to make a small microcontroller that is as fast as practical given the Tiny Tapeout constraints, and sticking to a 2x2 tile size.
- Language: Verilog
- License: Apache 2.0
- Author: Michael Bell

## Microcontrollers

[BOOM](https://github.com/riscv-boom) (Berkeley Out-of-Order Machine) [paper BOOMv2](https://www2.eecs.berkeley.edu/Pubs/TechRpts/2017/EECS-2017-157.pdf), out-of-order core with L1 cache. [BOOMv3](https://github.com/riscv-boom/riscv-boom/releases/tag/v3.0.0) can achieve 6.2 CoreMark/MHz
- License: BSD 3-Clause + Apache 2.0 for SiFive part
- Author: Berkeley
- Tools: [Chipyard Framework](https://github.com/ucb-bar/chipyard) to build BOOM

[Core-V](https://github.com/openhwgroup/core-v-docs) (fork of OpenRISC based, OR10N CPU)
- [CV32E40P (formerly RI5CY)](https://github.com/openhwgroup/cv32e40p) RV32IM[F]C, 4 stages
- [CVA6 (formerly Ariane)](https://github.com/openhwgroup/cva6) RV64IMAC+MSU, 6 stages
- [Ibex (formerly Zero-riscy)](https://github.com/lowRISC/ibex) RV32IEMCB, 2 stages
- Language: SystemVerilog
- License: Solderpad (Open Source), Ibex: changed from Solderpad to Apache 2.0, CVA6: Solderpad + Apache 2.0 from SiFive + Berkeley
- Author: [PULP Platform](https://www.pulp-platform.org/) (ETH Zurich, Università di Bologna)
- Peripherals: I²C, SPI, HyperRAM, GPIO

[DarkRISCV](https://github.com/darklife/darkriscv) implementation in one night of RISC-V in Verilog
- License: BSD 3-Clause
- Author: Marcelo Samsoniuk
- Language: Verilog

[DTU-mikrochip]()
- License:
- Author: Danmarks Tekniske Universitet (Danemark)


[FemtoRV32](https://github.com/BrunoLevy/learn-fpga/blob/master/FemtoRV/README.md) a minimalist RV32 core in the educational project [Learn FPGA](https://github.com/BrunoLevy/learn-fpga/) several versions, RV32I, RV32IM, intermissium (RV32IM + irq), gracilis (RV32IMC + irq), RV32IMFC + irq, Individua (RV32-IMAC)
- License: Three-clause BSD license
- Language: Verilog
- Author: Bruno Levy (in cooperation with Mecrisp)
- Board (FPGA): ULX3S, ECP5, FOMU, ARTY (+LiteX), Nandland Go board with HX1K FPGA
- Features: Several interesting examples, like HDMI, [NandLand RISC-V](https://github.com/Mecrisp/Nandland-RISC-V) (some characteristics of a retro-computer, UART + VGA)

[Fire-V](https://github.com/sylefeb/Silice/blob/master/projects/fire-v/doc/fire-v.md) RV32I, ~2K LUT (on ECP5),  hardware triangle rasterizer
- Four variants: Spark (minimalist), Blaze (geared towards IceBreaker), Wildfire (geared towards ULX3S), Inferno (dual-core version of Wildfire -- needs update
- Language: [Silice](https://github.com/sylefeb/Silice/)
- Author: sylefeb
- License: ??
- Boards: ECP5
- Features: BRAM, Pipelined SDRAM controller, SDRAM framebuffer (24bits RGB, double buffer, 640x480), hardware triangle rasterizer

[Ibtida](https://github.com/merledu/Ibtida), SoC based on [Buraq-mini](https://github.com/merledu/buraq_mini) RV32, 5 stage pipelined core, for IoT, derived from Nuecleus (see Humingbird) Some [Single Cycles Cores in Chisel](https://github.com/merledu/Riscv-Single-Cycle-Cores) made by undergraduate student
- Language: Chisel, adapted to Verilator for simulation
- Author: [MERLedu](Merledupk.org) (Micro Electronics Research Laboratory, Pakistan)
- License: Apache 2.0
- Boards: ?
- Tools: [BURQ IDE](https://github.com/merledu/BURQ-IDE), [Caravan](https://github.com/merledu/caravan) API to help to write bus protocols
- Features: TileLink bus.

[NOEL-V](https://www.gaisler.com/index.php/products/processors/noel-v), used by European Space Agency for their satellites, evolution of SPARC-V based LEON. RV32IM,RV32IMAG,RV32GCH/RV64IMAC,RV64GCH GRFPU or NanoFPU, no B extension
- [GRLIB](https://www.gaisler.com/index.php/downloads/leongrlib)
- Author: ESA, CAES
- Language: VHDL
- License: [GPL](https://www.gaisler.com/index.php/information/ordering/orderlicensing?task=view&id=177), Buy a license for non-GPL development without releasing sources
- Features: AMBA AHB/APB bus (ARM open source specs standard)
- Boards: NOEL-ARTYA7 (Xilinx Artix-7 FPGA), NOEL-PF (Microchip PolarFire FPGA), NOEL-XCKU (Xilinx Kintex UltraScale FPGA)
- OS: VxWorks, Linux, RTEMS, GCC BareMetal, Zephyr
- [Discourse of the project](https://discourse.grlib.community/)
- SoC: [De-RISC (some specs here)](https://riscv.org/blog/2021/10/de-risc-the-h2020-project-which-will-create-the-first-risc-v-fully-european-platform-for-aerospace-celebrates-its-second-anniversary-ana-risquez-navarro-de-risc/) use it at least for it's H(ypervisor) extension. [Slide with more informations](https://open-src-soc.org/2021-03/media/slides/3rd-RISC-V-Meeting-2021-03-30-16h15-Nils-Johan-Wessman.pdf) Looks like [De-RISC official site](https://derisc-project.eu/) is a technical empty, marketing only one (or blocked y adblockrs).

Humingbird E203 (RV32)
- Language: Verilog, work with [iVerilog (iVerilog or Icarus Verilog)](http://iverilog.icarus.com/) (IcarusVerilog simulator), opensource and GTKWave
- [Hummingbird E203](https://github.com/SI-RISCV/e200_opensource), license Apache 2.0, work Nuclei EV Kit, Hummingbird EV Kit (Xilinx Artix-7 FPGA)
- [Hbird E203 + RT-Thread on Lichee Tang](https://github.com/wuhanstudio/hbird_e203_tang), license Apache 2.0, Lichee Tang board, (Anlogic EG4S20 FPGA)
- [Hummingbird E203v2](https://github.com/riscv-mcu/e203_hbirdv2), license Apache 2.0, Nuclei ddr200t and mcu200t board
- License: Apache 2.0
- Author: Nucleisys
- OS: RT-Thread
- Tools: [HBird SDK](https://github.com/riscv-mcu/hbird-sdk/), based on [Nuclei SDK](https://github.com/Nuclei-Software/nuclei-sdk), also used on Gigdevice GD32V (GD32VF103C), [Nuclei: development platform for PlatformIO](https://github.com/Nuclei-Software/platform-nuclei)
- See more generally for
- RISC-V MCU: [Gitee](https://gitee.com/riscv-mcu/), [Github](https://gitee.com/riscv-mcu/)
- Nuclei Opensource tools [Gitee](https://gitee.com/Nuclei-Software/), [Github](https://gitee.com/Nuclei-Software/) [Nuecleisys Site](https://www.nucleisys.com/download.php)
- Specs [English](https://doc.nucleisys.com/nuclei_spec/), [中文](https://www.riscv-mcu.com/quickstart-doc-u-nuclei_n_isa.html)
- Harware board with ASIC: [Sipeed Longan Nano](https://longan.sipeed.com/en/), [BronzeBeard assembler, with baremetal boot](https://github.com/theandrew168/bronzebeard) - [bare metal mandelbrot RV32](https://github.com/enthusi/mandelbrot_riscv_assembler), example of using this assembler. Pine64 [Pinecil](https://pine64.com/product/pinecil-smart-mini-portable-soldering-iron/) re-programmable solderer

LiteX/VexRiscv SoC with several possible bus (Wishbone, AXI, Avalon-ST, Interconnect), with basic functionnalities (RAM, ROM, Timer, UART, etc…) can be extended with the ecosystem of LiteX cores (DRAM, PCIe, Ethernet, SATA, etc…)
- Various other ISA including OpenRISC, LM32, Zynq, X86 (through a PCIe)
- Language: Python, [Spinal HDL](https://github.com/SpinalHDL/SpinalHDL), can transpile to VHDL/Verilog/(n)Migen/Spinal-HDL, work well with Verilator (and it allow to display graphics output in simulation).
- Author: EnjoyDigital
- [LiteX](https://github.com/enjoy-digital/litex/)
- [Several supported boards](https://github.com/litex-hub/litex-boards) with several Intel, Lattice, Microsemi and Xilinx (Intel) FPGA.	 
[NEORV32](https://github.com/stnolting/neorv32), (derived from [NEO430](https://github.com/stnolting/neo430)) CPU+SoC (AXI4-Stream bus compatible)
- License: BSD 3-Clause License
- [Documentaion](https://stnolting.github.io/neorv32/)
- Tested on Lattice iCE40
- OS: [Zephyr OS](https://docs.zephyrproject.org/latest/boards/riscv/neorv32/doc/index.html)
- Interaces: (UART, SPI, TWI / I²C), GPIO and PWM, smart LED interface (NEOLED)
- Bus: Wishbone b4 compatible (WISHBONE), wrappers, AXI4-Lite master interface, Avalon-MM master interface

PicoRV32/PicoSoC, size-Optimized RISC-V microcontroler CPU (RV32IMC), 750-2000 LUTs, full SoC with AXI4-Lite interface
- [PicoRV32](https://github.com/cliffordwolf/picorv32) default 7-Series Xilinx FPGA implementation, Verilog, ISC license.
- [PicoRV32 MXO2 + Baremetal](https://github.com/wuhanstudio/picorv32_MXO2) MXO2 board (Lattice MXO2 FPGA) implementation
- [PicoRV32 Tang + RT-Thread](https://github.com/wuhanstudio/picorv32_tang) Sipeed Litchee Tang board (Anlogic EG4S20 FPGA), Apache 2.0 license
- [PicoTiny for Tang Nano 9K](https://github.com/sipeed/TangNano-9K-example/tree/main/picotiny) (PocoTinY SoC for GW1NR-9)
- License: ISC or Apache 2.0 depending on implementation (see above)
- Author: 
- OS: Baremetal (MXO2), RT-Thread (Tang)

Riscy RV32IM + RV64G
- Target: Various Target, via FPGAmake (Xilinx FGPA)
- Language: Bluespec System Verilog (BSV).
- License: MIT
- Author: 

[Shakti E-Class](https://gitlab.com/shaktiproject/cores/e-class/blob/master/README.md), RV32IMAC/RV64IMAC 3-stage in-order core for RTOSs like FreeRTOS or Zephyr. (See also microprocessors section for Shakti CPU)
- Author: University of Madras (India)
- Language: Bluespec System Verilog (BSV)
- License: IIT Madras (Open source license)

[SERV](https://github.com/olofk/serv) "award-winning" bit-serial RISC-V core, can fit 8 cores in a $38 FPGA
- License: ISCa
- Author: 
- Language: Verilog
- Boards: TinyFPGA BX (), Icebreaker, Icesugar， iCEstick (ICE40), OrangeCrab, Arty A7 35T, DE0 Nano, DE10 Nano, DECA development kit,EBAZ4205 'Development' Board， SoCKit development kit， Saanlima Pipistrello (Spartan6 LX45 FPGA)， Alhambra II， Nandland Go Board， 
- OS: Zephyr RTOS
- Tools: Compatible Verilator Open Source simulator

[SweRV](https://github.com/chipsalliance/Cores-SweRV) EHX3 (RV64, closed ?) and [EH1](https://github.com/chipsalliance/Cores-SweRV),[EH2](https://github.com/chipsalliance/Cores-SweRV-EH2),[EL2](https://github.com/chipsalliance/Cores-SweRV-EL2) (RV32)
- Author: ChipsAlliance/Western Digital
- Language: System Verilog
- License: Apache 2.0
- Boards: [Western Digital disks](https://www.westerndigital.com/solutions/risc-v)

[T-Head OpenXuanTie](https://github.com/T-head-Semi) (see also microprocessors section for T-Head CPU)
- [OpenE902](https://github.com/T-head-Semi/opene902) RV32EMC
- [Wujian100_open](https://github.com/T-head-Semi/wujian100_open) (microcontroller)
- Warning: XT804 is based on C-SKY isa, not RISC-V one
- Language: Verilog
- Author: [T-Head Semiconductor](https://www.t-head.cn/) (China)
- License: Apache-2.0
- Boards (FPGA): compilable with synplify (several vendors including Achronix, Intel, Lattice, Microsemi and Xilinx) + Vivado (Xilinx)
- Boards (ASIC): with Bouffalo BL808 SoC, including both MCU and CPU cores ([Sipeed M1S](https://wiki.sipeed.com/hardware/en/maix/m1s/m1s_module.html) ([SDK](https://github.com/sipeed/M1s_BL808_SDK)), [Pine64 Ox64](https://wiki.pine64.org/wiki/Ox64)
- Tools: Compatible Verilator (open source) simulator, [T-Head GNU Tolchain](https://github.com/T-head-Semi/xuantie-gnu-toolchain), [T-Head SDK](https://occ.t-head.cn/community/download?id=575997419775328256)
- OS: [Linux on the BL808)[https://github.com/bouffalolab/bl808_linux]

[TinyRiscV](https://gitee.com/liangkangnan/tinyriscv)
* Language: Verilog, with [iVerilog](http://iverilog.icarus.com/) simulation
* Author: Liang Langnan
* License: Apache 2.0
* Baoards: [TinyRiscV Vivado](https://gitee.com/liangkangnan/tinyriscv_vivado) for Xilinx Artix-7 XC7A35T

[X-Core](https://github.com/ARC-MX/X-Core)
* Langage: Verilog
* Author: PerfXLab (include people coming from MIT + Chinese Academy of Science + Intel + NVidia), led the development of OpenBLAS, as well as the OpenCL module in OpenCV
* Features: 5-stage pipeline RV32IM, based on PULP-platform RI5CY and some SoC components & SDK from Hummingbird E200.
* License: Apache-2.0
* Boards (FPGA): [Perf-V](https://perfv.org/) (based on Xilinx Artix-7 FPGA, support also Hummingbird E200, Syntacore SCR1, and UCTechip WH32 RISC-V open source cores).

### PULP Platform:
[Snitch](https://github.com/pulp-platform/snitch)
- Author: PULP-Platform (ETH Zurich, Università di Bologna)
- Luanguage:
- License: Apache 2.0 (software, utils) , Solderpad (hardware)
- Tools: Verilator simulator(opensource), Bender ???
- Features: Snitch cluster, Integer core, ???

[Chesshire](https://github.com/pulp-platform/cheshire) RV64IMAC+MSU
- Author: PULP-Platform (ETH Zurich, Università di Bologna) after CVA6 core (see above)
- Luanguage: SystemVerilog
- License: SOLDERPAD HARDWARE LICENSE version 0.51 (Apache 2.0 like)
- Features: Linux capable

[Iguana](https://github.com/pulp-platform/iguana) derived from Cheschire [can be synthetiszd at more than 90% by Open Source tools in 2023]()https://peertube.f-si.org/videos/watch/ad84c907-d767-4468-a43d-b5cbd4e04bdb


## Personnal projects:

[ChiselV](https://github.com/carlosedp/chiselv)
- Language: Chisel
- Author: Carlos Eduardo
- License: CC-BY 4.0
- Boards: Tested on ULX3s board (Lattice ECP5 FPGA)
- Tools: Yosys (Open source), NextPNR (Open Source), both mutliFPGA.

[KianRiscV](https://github.com/splinedrive/kianRiscV) RV32IM
- Language: Verilog
- Author: Hirosh Dabui
- License: ISC License
- Boards: ICEBreaker
- Tools: Raytracer and Mandelbrot demos

[NaxRiscv](https://github.com/SpinalHDL/NaxRiscv) (RV32 RV64)IMASU Out of Order, Superscalar, MMU (SV32, SV39)
- Language: Scala
- Author: SpinalHDL
- License: MIT License
- Boards: LiteX
- Tools: Can run Linux

[RISCV-Core](https://github.com/ombhilare999/riscv-core) RV32RI
- Author: ombhilare999
- License: MIT
- Language: Verilog

[Ricardo-V](https://github.com/zxmarcos/riscado-v/) RV32I
- Author: rxmarcos
- License: BSD 3-Clause License
- Language: Verilog

[USTCRVSoC](https://github.com/WangXuan95/USTC-RVSoC) RV32I
- Author: Xuan Wang (| Ph.D. student of CS, USTC, Suzhou)
- License: ?
- Language: SystemVerilog
- Boards: Nexys4 (Xilinx), Arty7 (Xilinx), DE0-Nano (Altera)
- Fetures: DMA, UART, VGA
- Tools: USTCRVSoC-tool.exe (simulator, Windows only), simulation via closed source Vivado

[Snitch](https://github.com/pulp-platform/snitch)
- Author: PULP-Platform (ETH Zurich, Università di Bologna)
- Luanguage: 
- License: Apache 2.0 (software, utils) , Solderpad (hardware)
- Tools: Verilator simulator(opensource), Bender ???
- Features: Snitch cluster, Integer core, ???

[Chesshire](https://github.com/pulp-platform/cheshire) RV64IMAC+MSU
- Author: PULP-Platform (ETH Zurich, Università di Bologna) after CVA6 core (see above)
- Luanguage: SystemVerilog
- License: SOLDERPAD HARDWARE LICENSE version 0.51 (Apache 2.0 like)
- Features: Linux capable

[Iguana](https://github.com/pulp-platform/iguana) derived from Cheschire [can be synthetiszd at more than 90% by Open Source tools in 2023]()https://peertube.f-si.org/videos/watch/ad84c907-d767-4468-a43d-b5cbd4e04bdb

[Warp-V](https://github.com/stevehoover/warp-v.git)
- Author: Steve Hoover
- License: BSD Clause3
- Language: TL-Verilog



## Closed source softcore for FPGA

Note: Closed source or didn't found sources.

[MI-V](https://www.microsemi.com/product-directory/fpga-soc/5210-mi-v-embedded-ecosystem)
- Language: ??? Proprietary
- Author: MicroSemi
- License: Proprietary ?
- Board: PolarFire SoC FPGA
- OS/ [LiteOS](https://github.com/RISCV-on-Microsemi-FPGA/LiteOS)

## Closed source microcontrollers on ASIC

Note: Closed source or didn't found sources.

Bouffalo Lab [BL602/BL604](https://www.bouffalolab.com/bl602) ([based on SiFive E2-Series](https://www.sifive.com/press/bouffalo-lab-standardizes-on-sifive-risc-v-embedded), [BL70X (BL702,BL704,BL706)](https://www.bouffalolab.com/bl70X) RV32+FPU (They also use open source T-Head cores in BL808, see above)
- Author: [Bouffalo Lab](https://www.bouffalolab.com/)
- Tools: [official SDK](https://github.com/bouffalolab/bl_iot_sdk), [open source firmware compilation and installation](https://lupyuen.github.io/articles/flash#flash-bl602-firmware-with-linux-macos-and-windows)
- Features: BL60X: (open source firmware), crypto, 2.4G WiFi 802.11 b/g/n BlueTooth LE 5.0, 276KB SRAM; BL70X: 2.4G WiFi, BL LE, Zigbee 3.0, Crypto, 
- Boards: BL602: Pine64 [PineCone](https://pine64.com/product/pinecone-bl602-evaluation-board/) , BL702: Sipeed [RV-Debugger Plus](https://tang.sipeed.com/en/hardware-overview/rv-debugger/) (USB2serial+JTAG)

Espressif [ESP32-C3](https://www.espressif.com/en/support/documents/technical-documents?keys=&field_type_tid[]=785) RV32IMC, [ESP32-H2](https://www.espressif.com/en/news/ESP32_H2), would be an improved version
- Author: [Espressif](https://www.espressif.com/)
- Tools: [ESP-IDF](https://github.com/espressif/esp-idf) (Apache 2.0), usr FreeRTOS, [esptool](https://github.com/espressif/esptool) (GPLv2), [OpenOCD ESP32](https://github.com/espressif/openocd-esp32) (GPLv2), all open source, but firmwares of some parts could be closed (need to verify), Arduino IDE ESP32-C3 lib, Apache NuttX
- Boards: ESP32-C3 DevKit (NodeMCU)
- Features: RV32IMC, crypto, WiFi 2.4GHz, Bluetooth BLE 5.0 (ESP-H2: BLE 5.2), 400 KB of on­chip SRAM, 8 KB of RTC FAST SRAM, C3F(N/H)4  models have 4MB of embedded flash

NSI-TEXE [DR1000C](https://www.nsitexe.com/en/data-flow-processor) (Data flow processor), contain MIMD based Vector extension
- Author: (NSI-TEXE)[https://www.nsitexe.com/]
- Implementations: used as accelerator in Renesas [RH850/U2B](https://www.renesas.com/sg/en/products/microcontrollers-microprocessors/rh850-automotive-mcus/rh850u2b-zonedomain-and-vehicle-motion-microcontroller). It is not RISC-V but instead up to 8 cores [RH850](https://www.renesas.com/us/en/products/microcontrollers-microprocessors/rh850-automotive-mcus) G4MH@400Mhz (V850/RH850 architecture) + RISCV vector processor as accelerator, Zone/Domaind and vehicle motion controller

## Other
[Building a vacuum tube computer](https://www.ludd.ltu.se/~ragge/vtc/) based on RISC−V RV32I at 1Mhz
- Author: Anders "ragge" Magnusson

## Microprocessors

[Caravel SoC Now](https://github.com/merledu/caravel_soc_now) (RV32i) an open source SoC with set of tool to help building SoCs in Chisel
- License: Apache 2.0
- Author: [Micro Electronics Research Laboratory (MERL)](https://merledupk.org), UIT University, Karachi, Pakistan
- Tools: [SoC-Now documentation](https://github.com/shahzaibk23/SoC-Now) An open source Mini SoC Generator which will generate SoC based on parameters

[ORV64](https://gitlab.com/picorio/OpenSource/hardware/orv64) RV64IMAC Core designed for embedded applications, 5 stage in-order pipeline and multi-level cache system including
-L1 I/Dcache and L2 I/D cache
- Language: SystemVerilog
- Author: PircoRio project
- License: BSD 3-Clause "New" or "Revised" License
- Features: Compatible Verilator (with SystemVerilog Translator and simulator) (open source) and VCS (closed source), GTKWave (open source)
- OS: Linux

[OpenXiangShan](https://github.com/OpenXiangShan)
- [XiangShan](https://github.com/OpenXiangShan/XiangShan)
- [fudian - High Performance IEEE-754 Floating-Point Unit](https://github.com/OpenXiangShan/fudian)
- Language: Chisel
- Authors: Institute of Computing Technology, Chinese Academy of Sciences, Peng Cheng Laboratory
- License: Mulan (open source), OSI approved
- Features: L2 Cache/LLC (from SiFive), FPU (Berkeley hardfloat), bus: Diplomacy/TileLink
- OS: Linux

[PicoRio](https://gitlab.com/picorio/), including [Sail-RVV](https://gitlab.com/picorio/sail-rvv) 
- Language: .Sail ???
- Authors: PicoRIo project, SRI International and the University of Cambridge Computer Laboratory under DARPA/AFRL contract
- License: BSD 2-clause
- Board: [Pygmy ES1Y](https://picorio-doc.readthedocs.io/en/latest/hardware/board.html) (WIP ?)

[RISC-V Rocket Core](https://github.com/chipsalliance/rocket-chip/tree/master/src/main/scala/rocket), inside [Rocket Chip Generator](https://github.com/chipsalliance/rocket-chip) (RV32/RV64)
- Langage: Chisel
- Authors: ChipsAlliance (derivated from Berkeley University)
- SiFive core : License Apache 2.0 
- JTAG et tools : ([License Berkeley ?](https://github.com/chipsalliance/rocket-chip/blob/master/LICENSE.Berkeley))
- OS: Linux

[Shakti](http://shakti.org.in/processors.html) various class of microprocessors
- Author: University of Madras (India)
- Language: Bluespec System Verilog (BSV)
- License: IIT Madras (Open source license) + BSD 3 Clause
- Features: M-Class and S-Class have 3 levels of cache, H-Class (targeting HPC) has 4 levels and up to 128 cores/SoC
- Tools: Use PlatformIO IDE, Arduino and plugin in Visual Studio, Shakti SDK.
- OS: GNU/Linux and Sel4

[T-Head OpenXuanTie](https://github.com/T-head-Semi) (See also microcontroller section for T-Head MCU)
- [OpenC910](https://github.com/T-head-Semi/openc910) RV64IMAFDC
- [OpenC906](https://github.com/T-head-Semi/openc906) RV64IMAFDC (AllWinner D1 implementation: rv64IMAFDVCU)
- Language: Verilog
- Author: [T-Head Semiconductor](https://www.t-head.cn/) (China)
- License: Apache-2.0
- FPGA Boards: ? compilable with synplify (several vendors including Achronix, Intel, Lattice, Microsemi and Xilinx) + Vivado (Xilinx)
- ASIC SoC: [AllWinner D1s/F133-A](https://www.allwinnertech.com/index.php?c=product&a=index&id=101) ; [AllWinner D1](https://www.allwinnertech.com/index.php?c=product&a=index&id=97) (RV64IMAFDCVU) [Official documenation](https://d1.docs.aw-ol.com/en/) [Linux-Sunxi documentation](https://linux-sunxi.org/D1), [T-Head TH1520](https://www.cnx-software.com/2022/10/04/alibaba-t-head-th1520-risc-v-processor-to-power-the-roma-laptop/) (4 C910 + 2 (various) to 64 cores), [Sophgo SG2042](https://www.sophon.ai/product/introduce/sg2042.html#feature) (use C920 variant of C910), 
- D1s/F133A boards: [MangoPi-MQ1](https://mangopi.org/mangopi_mq) SBC (AllWinner D1s/F133-A), [Yuzuki Nezha](https://bbs.aw-ol.com/topic/922/开源-yuzukinezha-d1s-核心板-mini-pice-核心板), [Quanzhi D1s-Nzeha](https://github.com/YuzukiHD/Nezha-D1s)
- D1 boards: Sipeed Nezha/AllWinner D1 board (dev SBC), Sipeed LicheeRV (headless SOM, 16$ board), [Xassette-Asterisk](https://www.cnx-software.com/2021/10/30/open-source-hardware-allwinner-d1s-risc-v-linux-sbc/) [T-Head RVB-ICE RISC-V SBC](https://occ.t-head.cn/community/risc_v_en/detail?id=RVB-ICE) ([article & links](https://www.electronics-lab.com/meet-alibaba-t-head-rvb-ice-risc-v-sbc-supports-android-10-debian-11-and-3d-gpu-acceleration/)), [MQ-Pro V1.1](https://nitter.fdn.fr/mangopi_sbc/status/1485505493405732864) (D1)
* TH1520 boards: [Lichee Pi 4A](https://wiki.sipeed.com/hardware/en/lichee/th1520/lp4a.html) [BeagleV](https://www.beagleboard.org/boards/beaglev-ahead)
- Tools: Compatible Verilator (open source) simulator, [T-Head GNU Tolchain](https://github.com/T-head-Semi/xuantie-gnu-toolchain), [T-Head SDK](https://occ.t-head.cn/community/download?id=575997419775328256)
- OS: Android, GNU/Linux,  [Yocto](https://github.com/T-head-Semi/xuantie-yocto), [Apache NuttX](https://github.com/apache/incubator-nuttx/tree/master/boards/risc-v/c906/smartl-c906), [Building own kernel and Debian system for Lichee RV](https://andreas.welcomes-you.com/boot-sw-debian-risc-v-lichee-rv/)

[TordBoyau](https://github.com/BrunoLevy/TordBoyau) RV32IM
- Author: Bruno Levy, INRIA Nancy
- language: Verilog
- License: BSD 3-Clause License
- Fatures: Pipeline
- Tools: The author made also the interesting [TinyPrograms](https://github.com/BrunoLevy/TinyPrograms) nice and beautiful resources to test core on terminal
- Other: [LearnFPGA](https://github.com/BrunoLevy/learn-fpga) by the same author, to create a FemtoRV minimalistic RISC-V CPU


[VexRiscV](https://github.com/SpinalHDL/VexRiscv) RV32MAFDC, seel also LiteX
- Author: LiteX
- Language: SpinalHDL
- microcontroller OS: Zephyr
- microprocessor OS: ([Linux compatible version](https://github.com/litex-hub/linux-on-litex-vexriscv), Migen Python DSL, tested with Intel, Lattice, Microsemi and Xilinx FPGA)

[VRoom!](https://moonbaseotago.github.io/) [RV64-IMAFDCHBK(V)](https://moonbaseotago.github.io/about.html), RV32 compatibility, the site include an interesting blog
- Author: Moonbase Otago
- License: GPL3, dual licensing possible

## Closed source microprocessors

Note: Closed source or didn't found sources.

- [SiFive](https://www.sifive.com/core-designer) E7 (RV32IMAFC), S7 and U7 Core IP Series (RV64GC/)
- Author: SiFive
- SoC: StarFive JH7100 Vision SoC
- Boards: VisionFive V1


# [Accelerators](#Accelerators)

[Ara](https://github.com/pulp-platform/ara) and [Spatz](https://github.com/pulp-platform/spatz), two implementations of the RISC-V Vector (RVV) 1.0 extension , made to be used as a coprocessor of respectively, CORE-V's [CVA6 core](https://github.com/openhwgroup/cva6) ([PULP fork)(https://github.com/pulp-platform/cva6) and [Snitch](https://github.com/pulp-platform/snitch_cluster).
- Language: Verilog
- Author: PULP Project (ETH Zurich and the University of Bologna)
- License: Apache 2.0

[BARVINN](https://github.com/cmcmicrosystems/BARVINN) [doc](https://barvinn.readthedocs.io/en/latest/) A Barrel RISC-V Neural Network Accelerator (looks like [NeuroMatrix architecture](https://www.semanticscholar.org/paper/NeuroMatrix®-NM6403-DSP-with-Vector%2FMatrix-engine-Fontine-Tchernikov/60e3c79a807495dbd318b5d7058b6879fa207d2f))
- Language: SystemVerilog (+ C and Python for usage)
- Author: CMC Microsystems (Canada)
- License: MIT

[Gemmini](https://github.com/ucb-bar/gemmini) ([doc](https://chipyard.readthedocs.io/en/latest/Generators/Gemmini.html)) systolic-array based matrix multiplication accelerator generator, RoCC accelerator with non-standard RISC-V custom instructions
- Language: Chisel (Scala), compatible with Chupyard & Spike
- Author:
- License:  ([Berkeley ?](https://github.com/ucb-bar/gemmini/blob/master/LICENSE))

[Hwacha](https://github.com/ucb-bar/hwacha) ([paper](https://www2.eecs.berkeley.edu/Pubs/TechRpts/2015/EECS-2015-262.pdf)), vector-fetch architecture
- Language: Scala (Chisel?), utilise [Chipyard](https://github.com/ucb-bar/chipyard)
- Author: University of Berkeley
- License: ?

[NVDLA](http://nvdla.org/) ([code](https://github.com/nvdla/hw)) NVIDIA Deep Learning Accelerator, user Rocket Core, simulation use fork of the FireSim FPGA-accelerated
- Language: Verilog
- Author: NVidia
- License: NVIDIA Open NVDLA License and Agreement v1.0
- Tools: [Firesim-nvdla](https://github.com/nvdla/firesim-nvdla) (simulator derived from [Firesim](https://github.com/firesim/firesim) (Chisel/Scala))

[VeriGPU](https://github.com/hughperkins/VeriGPU) Machine Learning oriented GPU, based on RISC-V
- Language: SystemVerilog
- License: MIT
- Tools: Python language examples and tools

[Vortex](https://vortex.cc.gatech.edu/) [Now V2.0](https://vortex.cc.gatech.edu/news/) [sources](https://github.com/vortexgpgpu/vortex) (RV32IMF up to 64 cores), "Scalable Multicore RISC-V GPGPU Accelerator for High-End FPGAs" [presentation](https://vortex.cc.gatech.edu/publications/hotchips-poster.pdf) [official site](https://vortex.cc.gatech.edu/) [tutorials](https://github.com/vortexgpgpu/vortex_tutorials) (Toolchain-prebuilt](https://github.com/vortexgpgpu/vortex-toolchain-prebuilt)
- Author: Multi authors
- License: BSD 3-Clause "New" or "Revised" License
- [Vortex tutorials](https://github.com/vortexgpgpu/vortex_tutorials)
- Language: SystemVerilog (after doc, OpenCL (use POCL, LLVM, RISC-V GNU Toolchain, Verilator)
- Tools: support OpenCL/Cuda, [NVPTX-SPIR-V Translator](https://github.com/vortexgpgpu/NVPTX-SPIRV-Translator), from [NVPTX](https://llvm.org/docs/NVPTXUsage.html) modified from LLVM-SPIR-V Translator.
- Features: OpenCL 1.2 support, up to 64 cores
- FPGA supported: Intel Arria 10 and Strafix 10

# [Simulators](#Simulators)

## Simulators
[Firesim](https://github.com/firesim/firesim)
- Language: Scala
- License: ([Berkeley ?](https://github.com/firesim/firesim/blob/master/LICENSE))

[Jupiter](https://github.com/andrescv/Jupiter) RV32IMF
- Language: Java
- License: GPLv3.0

[RARS](https://github.com/TheThirdOne/rars), RISC-V Assembler and Runtime Simulator (RARS) (RV32/RV64)IMFDN derivated from [MARS](http://courses.missouristate.edu/KenVollmar/mars/index.htm) (MIPS Assembler and Runtime Simulator)
- Author: 
- Language: Java
- License: MIT

[Ripes](https://github.com/mortbopet/Ripes), visual computer architecture simulator and assembly code editor for RISC-V RV32IMC/RV64IMC
- Author: Morten Borup Petersen (Denemark)
- Language: C++
- License: MIT
- Features: Render circuit schematic and transferts via [VSRTL](https://github.com/mortbopet/VSRTL), allow to test different kind of cache influence on performances, How C+asm code is compiled, interaction betwwen processor and memory mapped I/O, Qt Interface
- *Bonus:* Authors also make a very small footprint CPU called [Leros](https://leros-dev.github.io/) in about the same spirit than RISC-V

[Spike RISC-V ISA Simulator](https://github.com/riscv-software-src/riscv-isa-sim)
- Author:
- Language: C, assembly
- License: ([Berkeley ?](https://github.com/riscv-software-src/riscv-isa-sim/blob/master/LICENSE))

[SweRV-ISS](https://github.com/chipsalliance/SweRV-ISS) Whisper RISC-V instruction Set Simulator (ISS), for Swerv micro-controller
- Author: 
- Language: C++
- License: Apache 2.0

[Venus](https://github.com/ThaumicMekanism/venus) ([Backend](https://github.com/ThaumicMekanism/venusbackend)) RV32IM
- Author:
- Language: Kotlin (Java) + Web version
- License: MIT

## FPGA simulators
- [Verilator](https://www.veripool.org/verilator/) OpenSource general FGPA simulator (transpile in C, then in local assembler for fast simulation)
- [iVerilog or Icarus Verilog](http://iverilog.icarus.com/) older and slower Open Source simulator than Verilator

# Emulators

- [JuiceVM](https://github.com/juiceRv/JuiceVm) RV64IMASU, support FreeRTOS, RT-Thread, RT-SMART
- [LiteX](https://github.com/enjoy-digital/litex), works with Verilator (FGPA Simulator/debugger)
- [Qemu](https://wiki.qemu.org/Documentation/Platforms/RISCV) rv64imafdcsu (The reference for systems emulators and Virtual Machine)
- [Nemu](https://gitee.com/OpenXiangShan/NEMU) RV32IM
- [RVVM](https://github.com/LekKit/rvvm) RV64IMAFDC & RV32, tracing JIT (working on several archs), Working Linux, FreeBSD, OpenBSD, (+Haiku WIP), Graphical framebuffer through X11/WinAPI/Haiku/SDL + I2C HID
- [TinyEmu](https://bellard.org/tinyemu/) made by the author of Qemu, [example of Doom + BareBoxOS in TinyEMU in WASM](https://barebox.org/jsbarebox/?graphic=1)

# Tools

- [5% missing effort](https://lists.riscv.org/g/software/message/174) (Arch Linux: [doc](https://github.com/felixonmars/archriscv-packages/wiki) [current state](https://mirror.iscas.ac.cn/archriscv/.status/status.htm), Debian: [doc](https://wiki.debian.org/RISC-V) [current state](https://udd.debian.org/cgi-bin/ftbfs.cgi?arch=riscv64))
- RISC-V GNU Compiler Toolchain can be used to compile code for all RISC-V RV32 and RV64 cores.
- BitStream opensource documenting/synthesizing
  - [Yosys](http://www.clifford.at/yosys/) formal verification + synthesizing (open source), [YoWASP](http://yowasp.org/) (WebAssembly version)
    - [Apicula/Apycula](https://github.com/YosysHQ/apicula) for Gowin/Gow1n FPGA bitstream
    - [Icestorm](https://github.com/YosysHQ/icestorm/) for iCE40 FPGA bitsream
    - [Project Treillis](https://github.com/YosysHQ/prjtrellis/) for ECP5 FPGA bitstream
    - [Project Oxide](https://github.com/gatecat/prjoxide) for Lattice's 28nm "Nexus" FPGA bitstream
    - See also: [FPGA interchange schema definitions format](https://github.com/SymbiFlow/fpga-interchange-schema)
- [BronzeBeard](https://github.com/theandrew168/bronzebeard) RISC-V (RV32IMAC) assembler that integrate output in BareMetal OS (work with GD32V, including Longan Nano)
- [Connectal](https://github.com/csail-csg/connectal/) provides a hardware-software interface for applications split between user mode code and custom hardware in an FPGA
- [Deep in riscv debut](https://gitee.com/liangkangnan/deep_in_riscv_debug), JTAG interface for RISC-V
- [GRMON](https://www.gaisler.com/index.php/downloads/debug-tools) (Linux & Windows), debug monitor for LEON & NOEL-V
- [FireMarshal](https://github.com/firesim/FireMarshal/) builds base images for several linux-based distros that work with qemu, spike, and firesim
- [FPGAmake](https://github.com/cambridgehackers/fpgamake/) Generates Makefiles to synthesize, place, and route verilog using Vivado (Xilinx)
- [LiteScope](https://github.com/enjoy-digital/litescope) small footprint and configurable Logic Analyzer core powered by Migen & LiteX (by EnjoyDigital)
- [NextPNR](https://github.com/YosysHQ/nextpnr) routing (open source)
- [OpenESP](https://www.esp.cs.columbia.edu/) (not related to Espressif products like ESP32) An open plateform for SoC production, help to design from FPGA to ASIC, RTL: high-level synthesis (HLS), machine learning frameworks.
    - Available processors (all from Core-V, see above): 
        - 64-bit [Ariane](https://github.com/openhwgroup/cva6) (OpenHWGroup CVA6 RISC-V) RV64IMAC+MSU
        - 32-bit [Leon3](https://www.gaisler.com/index.php/products/processors/leon3) (Sparc)
        - 32-bit [Ibex](https://github.com/lowRISC/ibex) (LowRISC RISC-V)
    - Language: SystemC, C/C++, Chisel, NVDLA
    - FPGA boards: Based on Xilinx, ProFPGA Virtex7 and Virtex UltraScale
- [OpenFPGAloader](https://github.com/trabucayre/openFPGALoader) for flashing FPGA via USB (open source)
- [OpenSBI](https://github.com/riscv-software-src/opensbi) RISC-V Open Source Supervisor Binary Interface (OpenSBI) used as a standard boot
- [OreBoot](github.com/oreboot/oreboot) Rust (without C language) version of CoreBoot, manage ARM, RISC-V and few other architectures
- [platformIO](https://platformio.org/) ([core sources](https://github.com/platformio/platformio-core))
    - [ESP32](https://platformio.org/platforms/espressif32) include both xtensa ESP32-S3 and RISC-V ESP32-C3
    - [GD32V](https://platformio.org/platforms/gd32v) GD32VF103V-EVAL, Sipeed Longan Nano, Sipeed Longan Nano Lite, Wio Lite RISC-V
    - [Kendryte K210](https://platformio.org/platforms/kendryte210)
- [Rocket Chip Generator](https://github.com/chipsalliance/rocket-chip) Chisel see Microprocessors part.
- [VTR (Verilog To Routing)](https://verilogtorouting.org/) (open source)

## MicrocontrollerOS

BareMetal (No OS)
- [BareBox](https://www.barebox.org/doc/latest/boards/riscv.html) Qemu, TinyEMU (including WASM vrsion), BeagleV board, DE0-Nano FGPA board
- [BareMetal Newlib](https://github.com/ReturnInfinity/BareMetal-newlib)
- [ Baremetal Rust for RISC-V](https://speakerdeck.com/tomoyuki/baremetal-rust-for-risc-v)  
- [BronzeBeard](https://github.com/theandrew168/bronzebeard) RISC-V assembler (supporte RV32IMAC) that integrate output in BareMetal (work with GD32V, including Longan Nano and WIO-Lite boards)
- [Renode](https://github.com/y2kbugger/baremetal-riscv-renode) RISC-V BareMetal environment emulator
- [RISC-V: A Baremetal Introduction using C++. Intro](https://philmulholland.medium.com/modern-c-for-bare-metal-risc-v-zero-to-blink-part-1-intro-def46973cbe7) (Serie of tutorials about coding C++ Baremetal on RISC-V)
- [Bare metal RISC-V GCC 9.3.0 cross compiler instructions](https://xw.is/wiki/Bare_metal_RISC-V_GCC_9.3.0_cross_compiler_instructions)
- [ESP-IDF, need version >=4.3](https://github.com/espressif/esp-idf) I [made a tutorial](https://popolon.org/gblog3/?p=1881), [ArchLinux AUR packag](https://aur.archlinux.org/packages/esp-idf/)

[Apache NuttX](https://nuttx.apache.org/) ([RISC-V section](https://nuttx.apache.org/docs/latest/platforms/risc-v/index.html)), [include sim, it's own simulator](https://nuttx.apache.org/docs/latest/platforms/sim/sim/index.html), RISC-V support started in NuttX v7.19

- Bouffalo Lab [BL602, BL604](https://nuttx.apache.org/docs/latest/platforms/risc-v/bl602/index.html) (recipes bl602evb)
- Esspressif [ESP32-C3](https://nuttx.apache.org/docs/latest/platforms/risc-v/esp32c3/index.html), (recipes esp32c3) with 1.2.0, RV32IMC
- [GreenWaves GAP8](https://nuttx.apache.org/docs/10.0.0/introduction/detailed_support.html#greenwaves-gap8) (recipes ?) since v7.27 (RV32IM), 1+8-core DSP-like RISC-V MCU, feature a RI5CY core called Fabric Controller (FC) +8 slower speed RI5CY (see Core-V in [microcontrollers](#microcontrollers)). The SoC is an implementation of th PULP platform, not sure this board is still supported, it is probablt replaced by OpenVega (see below)
- [LiteX on ARTY A7 (FPGA)](https://nuttx.apache.org/docs/10.0.0/introduction/detailed_support.html#litex-on-arty-a7) (recipes )
- Microchip [PolarFire](https://nuttx.apache.org/docs/latest/platforms/risc-v/mpfs/index.html) (recipes m100pfsevp)
- Open-ISA [OpenVega or RISC-V RV32M1 VEGA Board](https://open-isa.org/) (recipes rv32m1-vega) is in the recipes, but not in the doc, mix of PULP-Platform RI5CY (4 stages RV32IMC) + Zero-Riscy (RV32IEMC) + [NXP ARM MCU SoC](https://hackaday.com/2019/02/04/openisa-launches-free-risc-v-vegaboard/) with CortexM0 + ARM Cortex M4
- Sipeed [Maix bit](https://nuttx.apache.org/docs/10.0.0/introduction/detailed_support.html#greenwaves-gap8a) (recipes maix-bit), since v9.0 (Kendryte K210, 2×RV64IMAFDC+NPU+AudioPU+flexible FPIOA)
- Xiaomi smartl-c906 (?) (recipes smartl-c906), use T-Head OpenC906 core, there is a [Qemu emulation](https://occ.t-head.cn/community/download)

Arduino
- Bouffalo Bl_arduino for [BL706](https://github.com/strongwong/bl_arduino) for [BL702](https://github.com/strongwong/bouffalo_arduino_lib), [Arduino source to add for both](https://raw.githubusercontent.com/strongwong/bl_arduino/main/package_bouffalolab_bl706_index.json) (doesn't work on linux x86_64)
- [ESP32-C3](https://github.com/espressif/arduino-esp32) (ESP32-C3 are RISC-V RV32IMC, ESP32-S3 are Xtensa ISA) See (https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html) for installation
- [GD32V](http://bigbits.oss-cn-qingdao.aliyuncs.com/Arduino_for_GD32V/package_longduino_index.json)
- [Maixduino](https://github.com/UT2UH/Maixduino/blob/gh-pages/package_Maixduino_boards_index.json) (use Kendryte K210)

[FreeRTOS on RISC-V](https://www.freertos.org/Using-FreeRTOS-on-RISC-V.html) [Source code](https://www.freertos.org/a00104.html) [Github](https://github.com/FreeRTOS/FreeRTOS), [LTS Github](https://github.com/FreeRTOS/FreeRTOS-LTS)
- [RTOS Demo for RISC-V MiFive M2GL025 / Renode](https://www.freertos.org/RTOS-RISC-V-SoftConsole-Renode-SiFive.html)
- [SiFive HiFive1 RTOS demo](https://www.freertos.org/RTOS-RISC-V-FreedomStudio-IAR-HiFive-RevB.html)
- [RTOS Demo for Spike simulator](https://interactive.freertos.org/hc/en-us/community/posts/210030246-32-bit-and-64-bit-RISC-V-using-GCC)
- [RV32M1 VEGAboard Demo (RI5CY Core)](https://www.freertos.org/RTOS-RISC-V-Vegaboard_Pulp.html)
- [RTOS Demo for RISC-V QEMU sifive_e Model](https://www.freertos.org/RTOS-RISC-V-FreedomStudio-QMEU.html)

HarmonyOS/[LiteOS](https://github.com/riscv-mcu/kernel_liteos_m) [Documentation](https://device.harmonyos.com/en/docs/documentation/guide/overview-harmonyos-0000001111162012)
- [Hisilicon Hi3861 V100](https://device.harmonyos.com/en/docs/documentation/guide/quickstart-lite-steps-hi3861-setting-0000001105989316)

[LuatOS](https://wiki.luatos.com/), based on [Lua scripting language](https://www.lua.org/), [Sources](https://gitee.com/openLuat/LuatOS), Qemu for RISC-V, Air101/102 are C-Sky ISA based boards, there is an abstraction layer compatible with M3/armv7/risc-v/win32/posix
- License: MIT
- Board: [RISC-V based ESP32-C3 SoC](https://github.com/dreamcmi/LuatOS-ESP32), warning, ESP32-S3 are Xtensa ISA based, they are also supported by the same code.

[MicroPython](https://micropython.org/), reduced for embedded version of Python scripting language. See also [Developing Accelerators for Micropython on the RISC-V platform](https://www.sra.uni-hannover.de/Theses/2019/MA-profiling-micropython.html)
- [MaixPy](https://wiki.sipeed.com/soft/maixpy/en/) on Kendryte K210
- No firmware for [ESP32-C3](https://micropython.org/download/esp32c3-usb/), but there is a [ESP32](https://github.com/micropython/micropython-esp32/tree/esp32/ports/esp32), how much work [here is xtensa-esp32-elf- prefix](https://github.com/micropython/micropython-esp32/blob/esp32/ports/esp32/sdkconfig.h) (replace by riscv-elf- prefix ?, compile with esp-idf>=4.3 ?

[OpenWRT](https://openwrt.org/)
- [implementation](https://github.com/xfguo/riscv-openwrt)
- [Qemu RISC-V](https://openwrt.org/docs/guide-user/virtualization/qemu#openwrt_in_qemu_risc-v)
- [SiFive SoC](https://openwrt.org/docs/techref/hardware/soc/soc.sifive)

[RT-Thread](https://www.rt-thread.io/) ([Supported boards](https://www.rt-thread.io/board.html))
- [sources](https://github.com/RT-Thread/rt-thread) [SDK toolchain RISC-V GCC](https://github.com/RT-Thread-Studio/sdk-toolchain-RISC-V-GCC)
- supports Hifive (sources not found)
- [Qemu SDK debugger](https://github.com/RT-Thread-Studio/sdk-debugger-qemu)
- Kendryte: Windows binary for [SDK toolchain](https://github.com/RT-Thread-Studio/sdk-toolchain-riscv-kendryte) [debugger OpenOCD](https://github.com/RT-Thread-Studio/sdk-debugger-openocd-kendryte)
- Nuclei (including [GD32VF103v-eval](https://github.com/RT-Thread/rt-thread/tree/master/bsp/gd32vf103v-eval), [SDK BSP GD32Vf103 Nuclei RVStar](https://github.com/RT-Thread-Studio/sdk-bsp-gd32vf103-nuclei-rvstar), [Hbird E203 + RT-Thread on Lichee Tang](https://github.com/wuhanstudio/hbird_e203_tang)

RustOS/RISC-V OS in Rust
- [RVOS-RS](https://github.com/tomoyuki-nakabayashi/rvos-rs) RISC-V OS written in Rust, Lic. Apache 2.0
- [Makeing RISC-V OS using Rust](http://osblog.stephenmarz.com/index.html) (Tutorial about making an OS for RISC-V in Rust)

[Zephyr](https://www.zephyrproject.org/)
- [Qemu](https://risc-v-getting-started-guide.readthedocs.io/en/latest/zephyr-qemu.html)
- Zephyr LiteX/VexRiscv on [Qmtech Wukong Board](https://www.luffca.com/2021/10/zephyr-litex-vexriscv-wukong/)
- Zephyr LiteX/VexRiscv on [PolarFire FPGA](https://risc-v-getting-started-guide.readthedocs.io/en/latest/zephyr-litex.html)
- [SiFIve HiFive1](https://risc-v-getting-started-guide.readthedocs.io/en/latest/zephyr-hifive1.html)
- 
## MicroprocessorOS

[Android](https://plctlab.github.io/aosp/create-a-minimal-android-system-for-riscv.html) ([sources](https://github.com/aosp-riscv))
https://www.xda-developers.com/android-risc-v-port/) run on Qemu
- [XuanTie C910](https://github.com/T-head-Semi/riscv-aosp)

BSD
- [FreeBSD](https://wiki.freebsd.org/riscv)
- [NetBSD](http://wiki.netbsd.org/ports/riscv/) ([Building NetBSD on the RISC-V](https://github.com/zmcgrew/riscv-tools-netbsd))
- [OpenBSD](https://www.openbsd.org/riscv64.html)

[Haiku](https://discuss.haiku-os.org/t/my-haiku-risc-v-port-progress/10663), first one to be able to run accelerated GPU via Mesa

Linux
- [Arch Linux](https://github.com/archlinux-riscv) doesn't look very active, [packages tagged as RISC-V](https://aur.archlinux.org/packages/?K=risc-v&SB=p)
- [Debian](https://wiki.debian.org/RISC-V)
- [Fedora](https://fedoraproject.org/wiki/Architectures/RISC-V)
- [OpenEmbedded/Yocto](https://github.com/riscv/meta-riscv) including Distro-less (only with OE-Core), Yoe Distro, Yocto/Poky, [RVB-ICE](https://occ.t-head.cn/vendor/detail/index?id=3906780664213024768&vendorId=3706716635429273600&module=4) board [XT910 port](https://github.com/T-head-Semi/xuantie-yocto)
- [OpenEuler](https://gitee.com/openeuler/RISC-V) 
- [Ubuntu](https://wiki.ubuntu.com/RISC-V)

# ExtensionsNomenclature
There is a [page dedicated to recently ratified extension](https://wiki.riscv.org/display/TECH/2021+ISA+Ratifications+-+Waivers+Dashboard) still not integrated in specifications books.

* According to [riscv-privileged-20211203.pdf, p17](https://github.com/riscv/riscv-isa-manual/releases/download/Priv-v1.12/riscv-privileged-20211203.pdf)
- A - **A**tomics
- B - **B**it manipulation
- C - **C**ompression
- D - **D**ouble-precision floating point
- E - RV32 **E**mbedded
- F - single-precision **F**loating point
- G - **G**eneral, shorthand for IMAFD
- H - **H**ypervisor
- I - RV32I/RV64I/RV128I base **I**sa
- J - Tentatively reserved for **J**IT ([just-in-time compilation for script languages](https://github.com/riscv/riscv-j-extension))
- L - reserved (some said decima**l** floating point)
- M - integer **M**ultiply and division
- N - User level i**N**terrupt
- P - **P**acked SIMD
- Q - **Q**uad-precision floating point
- S - **S**upervisor
- T - **T**ransactionnal memory
- U - **U**ser mode implemented
- V - **V**ector processor
- X - non-standard e**X**xtensions

Some other (to be completed) in 20211203 + page dedicated for not integrated (link to specific specs in this case):
- Smepmp - [PMP Enhancements for memory access and execution prevention on Machine mode](https://github.com/riscv/riscv-tee/blob/main/Smepmp/Smepmp.pdf)
- Smstateen - RISC-V [State Enable Extension](https://github.com/riscv/riscv-state-enable/releases/download/v0.6.3/Smstateen.pdf)
- Sscofpmf - RISC-V [Count Overflow and Mode-Based Filtering](https://github.com/riscv/riscv-count-overflow/releases/download/v0.5.2/Sscofpmf.pdf)
- Sstc - RISC-V ["stimecmp / vstimecmp"](https://github.com/riscv/riscv-time-compare/releases/download/v0.5.4/Sstc.pdf)
- Sm1-12, Ss1-12, Sv57, Hypervisor, Svinval, Svnapot, Svpbmt - [The RISC-V Instruction Set Manual Volume II: Privileged Architecture](https://github.com/riscv/riscv-isa-manual/releases/download/draft-20211105-c30284b/riscv-privileged.pdf)
-- Svinval - fine-grained address-translation cache invalidation
-- Svnapot - virtual to physique address translation for address greater than base memory, with: naturally aligned at power of two (NAPOT)
-- Svpbmt - page based memory type
- Zam - support for misaligned AMO, not sure it is dedicated to this
- Zba, Zbb, Zbc, Zbs - [RISC-V Bit-Manipulation](https://github.com/riscv/riscv-bitmanip/releases/download/1.0.0/bitmanip-1.0.0-38-g865e7a7.pdf)
- Zbkb, Zbkc, Zbkx, Zknd, Zkne, Zknh, Zksed, Zksh, Zkn, Zks, Zkt, Zk, Zkr : RISC-V [Cryptography Extensions Volume I: Scalar & Entropy Source](https://github.com/riscv/riscv-crypto/releases/download/v1.0.0-rc6-scalar/riscv-crypto-spec-scalar-1.0.0-rc6.pdf)
- Zicbom, Zicbop, Zicboz - RISC-V [Base Cache Management Operation](https://github.com/riscv/riscv-CMOs/blob/master/specifications/cmobase-v1.0-rc2.pdf)
- Zfinx, Zdinx, Zhinx, Zhinxmin - ["Zfinx", "Zdinx", "Zhinx", "Zhinxmin": Standard Extensions for Floating-Point in Integer Registers](https://github.com/riscv/riscv-zfinx/blob/main/zfinx-1.0.0-rc.pdf)
- Zfh, Zfhmin - ["Zfh" and "Zfhmin" Standard Extensions for Half-Precision Floating-Point](https://drive.google.com/file/d/1GJb230aA2HKkRJ2jqw5bRL0q3FOPQMrN/)
- Zvamo, Zvlsseg, Zvediv, Zvqmac, Zve32x, Zve32f, Zve64x, Zve64f, Zve64d, Zvfh, Zve - [RISC-V Vector Extension](https://github.com/riscv/riscv-v-spec/releases/tag/v1.0)

# MoreDoc

[VRoom!](https://moonbaseotago.github.io/) is an interesting blog about producing a RISC-V core on FPGA.
[FPGA 101](https://github.com/litex-hub/fpga_101) Lessons: Discover FPGA, first design, create a Soft Cire, a SoC, 

